# OpenML dataset: Music-Dataset--1950-to-2019

https://www.openml.org/d/43845

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
This dataset provides a list of lyrics from 1950 to 2019 describing music metadata as sadness, danceability, loudness, acousticness, etc. Authors also provide some information as lyrics which can be used to natural language processing. 
Acknowledgements
Moura, Luan; Fontelles, Emanuel; Sampaio, Vinicius; Frana, Mardnio (2020), Music Dataset: Lyrics and Metadata from 1950 to 2019, Mendeley Data, V3, doi: 10.17632/3t9vbwxgr5.3

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43845) of an [OpenML dataset](https://www.openml.org/d/43845). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43845/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43845/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43845/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

